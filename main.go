package pkg

import "math"

func getFlags(a byte) []bool {
	flags := make([]bool, 8)
	for i:=7; a!=0; i-- {
		val := a % 2 == 1
		a = a >> 1
		flags[i] = val
	}
	return flags
}

func bytesToInt(a []byte) int {
	var res int = 0
	l := len(a)
	for i:=0;i<l;i++ {
		res += int(a[l-i-1])*int(math.Pow(255, float64(i)))
	}
	return res
}

func intToBytes(a int) []byte {
	res := make([]byte, 0)
	o := 255
	for i:=0;a>0;i++ {
		x := int(math.Floor(float64(a / o)))
		y := byte(a % o)
		res = append(res, y)
		a = x
	}
	return res
}

func getSizes(a []byte, sl int, count int) []int {
	sizes := make([]int, count)
	for i:=0;i<count;i++ {
		f := (1+sl)+(i*sl)
		t := f+sl
		sizes[i] = bytesToInt(a[f:t])
	}
	return sizes
}

func GetData(a []byte) [][]byte {
	sizeLength := a[0] % 16
	count := bytesToInt(a[1:(1+sizeLength)])
	sl := int(sizeLength)
	sizes := getSizes(a, sl, count)
	data := make([][]byte, count)
	f := (1+sl)+(count*sl)
	for i:=0;i<count;i++ {
		t := f+sizes[i]
		data[i] = a[f:t]
		f = t
	}
	return data
}

func ToMap(a [][]byte) map[string][]byte {
	l := len(a)
	res := make(map[string][]byte)
	for i:=0;i<l;i+=2 {
		res[string(a[i])] = a[i+1]
	}
	return res
}

func Marshal(a [][]byte, flags []bool) []byte {
	max := len(a)
	for _, v := range a {
		l := len(v)
		if l > max {
			max = l
		}
	}
	sizeLength := byte(math.Ceil(math.Log2(float64(max)) / 8))
	var first byte = 0
	fl := 4
	if len(flags) < fl {
		fl = len(flags)
	}
	for i:=0;i<fl;i++ {
		var x byte = 0
		if flags[i] {
			x = 1
		}
		first += x*byte(math.Pow(2, float64(fl-1-i)))
	}
	first += 15 + sizeLength
	count := intToBytes(len(a))
	sizes := make([]byte, 0)
	for _, v := range a {
		sizes = append(sizes, intToBytes(len(v))...)
	}
	res := []byte{first}
	res = append(res, count...)
	res = append(res, sizes...)
	for _, v := range a {
		res = append(res, v...)
	}
	return res
}

func Validate(a []byte) bool {
	l := len(a)
	t := 1
	if l < t {
		return false
	}
	sizeLength := a[0] % 16
	sl := int(sizeLength)
	t += sl
	if l < t {
		return false
	}
	count := bytesToInt(a[1:1+sizeLength])
	t += sl*count
	if l < t {
		return false
	}
	sizes := getSizes(a, sl, count)
	for _, v := range sizes {
		t += v
	}
	if l < t {
		return false
	}
	return true
}

func GetFlags(a []byte) []bool {
	flags := getFlags(a[0] >> 4)[4:]
	return flags
}

// func main() {
// 	input := []byte{17,2,4,6,43,43,43,43,45,45,45,45,45,45}
// 	flags := GetFlags(input)
// 	fmt.Println("flags:", flags)
// 	data := GetData(input)
// 	for k, v := range data {
// 		fmt.Println(k, ":", string(v))
// 	}
// 	 fmt.Println(Marshal(data, flags))
// }
